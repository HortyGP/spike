'''
import http.cookiejar
import urllib.request
import urllib.parse
from base64 import b64decode as dtk
import ssl
ssl._create_default_https_context = ssl._create_unverified_context


def tk():
    return dtk(b'bGFiYXNlNGN0MXY=').decode("UTF8")


class ActivPipe:
    def __init__(self, url="https://activufrj.nce.ufrj.br"):
        self.url = url
        # cj = http.cookiejar.CookieJar()
        self.opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor())
        top_level_url = "https://activufrj.nce.ufrj.br/login"

        password_mgr = urllib.request.HTTPBasicAuthHandler()
        password_mgr.add_password(None, top_level_url, "carlo", tk())

        handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
        self.opener.add_handler(handler)
        self.opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        urllib.request.install_opener(self.opener)
        request = urllib.request.Request(url)
        resp = urllib.request.urlopen(request)
        self.cookie = [
            cook for key, cook in resp.getheaders() if key == 'Set-Cookie'][0].split("=")[1].split(";")[0]
        urllib.request.install_opener(self.opener)
        payload = {
            '_xsrf': self.cookie,
            'user': 'carlo',
            'passwd': tk()
        }
        data = urllib.parse.urlencode(payload)
        binary_data = data.encode('UTF-8')
        request = urllib.request.Request(top_level_url, binary_data)
        urllib.request.urlopen(request)

    def painter(self, url):
        url = self.url + url
        url = "https://activufrj.nce.ufrj.br/bookmarks/BibliotecaIES/_relatorio_"
        req = urllib.request.Request(url)
        resp = urllib.request.urlopen(req)
        print("resp =  request.Request", resp.getheaders())
        pg = str(resp.read())
        while pg:
            print(pg[:100])
            pg = pg[100:]
'''

LABASE_LNK = [dict(icon="flask", link="http://labase.superpython.net")]
LABASE = "http://labase.superpython.net"
LBL = dict(flask="labase", tint="lattes", github="github", gitlab="gitlab", facebook="facebook", book="livrozilla")
LOREMPIX = "http://lorempixel.com/400/200/business/{}"
DUMMY = "https://i.imgur.com/QtjhrpM.jpg"
LOREM = "Bacon ipsum dolor amet ball tip brisket salami doner swine tenderloin." \
        "  Sausage leberkas bacon pork belly boudin.  Short ribs meatloaf prosciutto tail sausage meatball." \
        "  Cow pork loin kielbasa beef fatback ham jowl shankle porchetta ground round jerky kevin." \
        "  Ground round ball tip pancetta, picanha jerky ham boudin ham hock kevin."
ID_DATA = [
    dict(
        name="Carlo E. T. Oliveira",
        role="Pesquisador",
        bio="Possui doutorado em Computação - University Of London (1991)."
            " Atualmente é um pesquisador da Universidade Federal do Rio de Janeiro."
            " Tem experiência na área de Ciência da Computação, com ênfase em Informática Educacional,"
            " Sistemas de informação e Engenharia de Software atuando principalmente nos seguintes temas:"
            " neuropedagogia, neurociência computacional, orientação a objetos, uml, sistemas distribuídos,"
            " arquitetura de software e sistemas peer-to-peer. ",
        home="http://activufrj.nce.ufrj.br/profile/carlo",
        links=[
            dict(icon="flask", link=LABASE),
            dict(icon="tint", link="http://lattes.cnpq.br/9627675808739540"),
            dict(icon="github", link="https://github.com/cetoli"),
            dict(icon="gitlab", link="https://gitlab.com/cetoli"),
            dict(icon="facebook", link="https://www.facebook.com/carlo.oliveira"),
        ],
        photo="http://activufrj.nce.ufrj.br/photo/carlo",
        back=LOREMPIX
    ),
    dict(
        name="Isabel Hortência G. P. B.",
        role="Mestranda",
        bio="Mestranda em Informática pela UFRJ, possui graduação em Desenho Industrial"
             " e em Licenciatura em Computação pela UFJF. Atualmente é Coordenador do Curso de Tecnologia "
             "em Sistemas da Computação pelo CEDERJ/UAB- UFF Universidade Federal Fluminense"
             " onde também atua como tutor. Atua como Docente de Informatica no SENAC em Cursos"
             " Técnicos e profissionalizantes. Foi instrutor de informática da"
             " Fundação de Apoio à Escola Técnica do Estado do Rio de Janeiro FAETEC."
             " Tem experiência na área de Educação, com ênfase em Educação, atuando principalmente"
             " no seguinte tema: ensino, Web 2.0, Educação a Distância e Informática na Educação. ",
        home="http://activufrj.nce.ufrj.br/profile/IsabelBarros",
        links=[
            dict(icon="flask", link=LABASE),
            dict(icon="tint", link="http://lattes.cnpq.br/2327365626384469"),
            dict(icon="facebook", link="https://www.facebook.com/isabel.barros.9047"),
        ],
        photo="http://activufrj.nce.ufrj.br/photo/IsabelBarros",
        back=LOREMPIX
    ),
    dict(
        name="TATIANA DE SOUSA RIBEIRO",
        role="Bibliotecária",
        home="http://activufrj.nce.ufrj.br/profile/tatiana.ribeiro",
        links=[
            dict(icon="flask", link=LABASE),
            dict(icon="book", link="http://livrozilla.com/doc/941726/ribeiro--tatiana-de-sousa"),
        ],
        photo="http://activufrj.nce.ufrj.br/photo/tatiana.ribeiro",
        back="http://lorempixel.com/400/200/business"
    )
]
SESSIONS = [
    dict(
        session="Biblioteca",
        icon="home",
        href="#"
    ),
    dict(
        session="Notícias",
        icon="bullhorn",
        href="#"
    ),
    dict(
        session="Ajuda",
        icon="question-circle",
        href="#"
    ),
    dict(
        session="Cadastro",
        icon="file-signature ",
        href="#"
    ),
    dict(
        session="Quem Somos",
        icon="info-circle",
        href="#"
    )
]

CARD = """
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="{back}" alt="{name}">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="{photo}" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">{name}</p>
                                    <p><span class="title is-6"><a href="{home}">@homepage</a></span></p>
                                    <p class="subtitle is-6">{role}</p>
                                </div>
                            </div>
                            <div class="content">
                                {bio}
                            </div>
                            <div class="content">
                                {icons}
                            </div>
                        </div>
                    </div>
                </div>

            """


BRY = object()


class Session(object):

    def paint(self, main_menu, id_cards, data=SESSIONS, id_data=ID_DATA):
        """
            <span class="navbar-item">
                <a class="button is-white is-outlined" href="#">
                    <span class="icon is-large">
                        <i class="fa fa-home"></i>
                    </span>
                    <span>Biblioteca</span>
                </a>
            </span>

        :param main_menu: Menu to be painted
        :param data: Data to be painted in
        :return: DOM item generated
        """
        html = BRY.HTML

        def do_item(session, icon, href):
            menu_item = html.SPAN(Class="navbar-item")
            a = html.A(Class="button is-white is-outlined", href=href)
            s = html.SPAN(Class="icon is-large")
            i = html.I(Class="fa fa-{}".format(icon))
            t = html.SPAN(session)
            menu_item <= a
            a <= s
            s <= i
            a <= t
            print("def do_item(session, icon, href):", session, icon, href, a.Class, s.Class, i.Class)
            return menu_item

        def do_card(name, role="Pesquisador", bio=LOREM, home=LABASE, links=LABASE_LNK, photo=DUMMY, back=LOREMPIX):
            """

            :param name: Nome Completo
            :param role: Papel profissional (Pesquisador, Mestrado, Egresso)
            :param home: Link do Activ
            :param links: Dicionário com outros links {LATTES: "", FACE: ""}
            :return:
            """
            back = back.format(role) if "{}" in back else back
            icon = '<a href="{link}" title={title}><i class="fa fa-{icon} fa-2x" aria-hidden="true"></i></a>'
            # icons = "\n".join(icon.format(title=ref["icon"], **ref) for ref in links)
            icons = " ".join(icon.format(title=LBL[ref["icon"]], **ref) for ref in links)
            card = dict(name=name, role=role, bio=bio, home=home, icons=icons, photo=photo, back=back)
            #print("icons = ", CARD, card.keys())
            return CARD.format(**card)

        navbar = html.DIV(Class="navbar-end")
        for item in data:
            navbar <= do_item(**item)
        #print("def paint(self, main_menu, data=SESSIONS):", data)
        main_menu.html = ""
        main_menu <= navbar
        id_cards.html = "\n".join(do_card(**ids) for ids in id_data)
        # print(id_data)
        # print("\n".join(do_card(**ids) for ids in id_data))
        return main_menu


def main(brython):
    global BRY
    BRY = brython
    Session().paint(BRY.main_menu, BRY.id_cards)


if __name__ == '__main__':
    from browser import document, html


    class Browser:
        DOC, HTML = document, html
        main_menu = document["nav-menu"]
        id_cards = document["people"]


    main(Browser)

CARD = """
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="{back}" alt="{name}">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="{photo}" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">{name}</p>
                                    <p><span class="title is-6"><a href="{home}">@homepage</a></span></p>
                                    <p class="subtitle is-6">{role}</p>
                                </div>
                            </div>
                            <div class="content">
                                {bio}
                            </div>
                            <div class="content">
                                {icons}
                            </div>
                        </div>
                    </div>
                </div>

            """
DIVISORIA = """
    <section class="hero is-info is-medium is-bold">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">{title}</h1>
                <a id="{section}" name="{section}">
            </div>
        </div>
    </section>
"""
ARTICLE = """
                        <div class="tile is-parent">
                            <a href="#{title}">
                                <article class="tile is-child">
                                    <p class="title">
                                        <span class="icon is-size-1">
                                                <i class="fa fa-{icon}"></i>
                                        </span>
                                    </p>
                                    <p class="subtitle">{title}</p>
                                </article>
                            </a>
                        </div>

"""