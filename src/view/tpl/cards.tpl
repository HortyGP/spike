<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Biblioteca de Informática Educação e Sociedade</title>
    <!-- stylesheets -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/file/BibliotecaIES/bulma.css">
    <link rel="stylesheet" href="/file/BibliotecaIES/style.css">
    <link rel="stylesheet" href="/file/BibliotecaIES/landing.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/site/css/cards.css">
    <script  type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {

            // Get all "navbar-burger" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
              $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

              });
            });
            }

        });
    </script>    <!-- fonts -->
    <script type="text/javascript" src="https://cdn.rawgit.com/brython-dev/brython/3.3.2/www/src/brython.js"></script>
    <script type="text/python" src="/file/BibliotecaIES/main.py?disp=inline">
    <script type="text/python">
        from browser import document, html

    </script>

</head>

    <body onLoad="brython({debug:1, cache:'browser', static_stdlib_import:true,
     pythonpath :['/file/BibliotecaIES/']})" bgcolor= darkslategray>

    <div class="container">
        <div id="flow">
            <span class="flow-1"></span>
            <span class="flow-2"></span>
            <span class="flow-3"></span>
        </div>
            <!-- Developers -->
        <section class="hero is-info is-fullheight">
            <div class="hero-head">
                <nav class="navbar">
                    <div class="container is-fluid">
                        <div class="navbar-brand">
                            <a class="navbar-item" href="../">
                                <img src="/file/BibliotecaIES/Logo-PPGI-80-3.png" alt="PPGI">
                            </a>
                            <span class="navbar-burger" data-target="nav-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </div>
                    <div id="nav-menu" class="navbar-menu">
                            <div class="navbar-end">
                                <span class="navbar-item">
                                    <a class="button is-white is-outlined" href="#">
                                        <span class="icon is-large">
                                            <i class="fa fa-home"></i>
                                        </span>
                                        <span>Biblioteca</span>
                                    </a>
                                </span>
                                <span class="navbar-item">
                                    <a class="button is-white is-outlined" href="#">
                                        <span class="icon">
                                            <i class="fa fa-bullhorn"></i>
                                        </span>
                                        <span>Notícias</span>
                                    </a>
                                </span>
                                <span class="navbar-item">
                                    <a class="button is-white is-outlined" href="#">
                                        <span class="icon">
                                            <i class="fa fa-question-circle "></i>
                                        </span>
                                        <span>Ajuda</span>
                                    </a>
                                </span>
                                <span class="navbar-item">
                                    <a class="button is-white is-outlined" href="#">
                                        <span class="icon">
                                            <i class="fa fa-info-circle"></i>
                                        </span>
                                        <span>Sobre Nós</span>
                                    </a>
                                </span>
                            </div>
                        </div>

                    </div>
                </nav>
            </div>

            <div class="hero-body">
                    <div class="container has-text-centered">
                        <div class="column is-6 is-offset-3">
                            <h1 class="title">
                                Biblioteca de Informática Educação e Sociedade
                            </h1>
                            <h2 class="subtitle">
                                Uma coleção de obras produzidas pelos pesquisadores da área.
                                Inclui uma coleção de obras de referência para uso limitado aos alunos da área.
                            </h2>
                 <section class="info-tiles">
                    <div id="section-menu" class="tile is-ancestor has-text-centered">
                        <div class="tile is-parent">
                            <a href="#livros">
                                <article class="tile is-child">
                                    <p class="title">
                                        <span class="icon is-size-1">
                                                <i class="fa fa-book"></i>
                                        </span>
                                    </p>
                                    <p class="subtitle">Livros</p>
                                </article>
                            </a>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child">
                                <p class="title">
                                    <span class="icon is-size-1">
                                            <i class="fa fa-file-alt "></i>
                                    </span>
                                </p>
                                <p class="subtitle">Artigos</p>
                            </article>
                        </div>
                        <div class="tile is-parent">
                            <a href="#autores">
                                <article class="tile is-child">
                                    <p class="title">
                                        <span class="icon is-size-1">
                                                <i class="fa fa-id-card"></i>
                                        </span>
                                    </p>
                                    <p class="subtitle">Autores</p>
                                </article>
                            </a>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child">
                                <p class="title">
                                    <span class="icon is-size-1">
                                            <i class="fa fa-clipboard"></i>
                                    </span>
                                </p>
                                <p class="subtitle">Relatórios</p>
                            </article>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child">
                                <p class="title">
                                    <span class="icon is-size-1">
                                            <i class="fa fa-file-contract"></i>
                                    </span>
                                </p>
                                <p class="subtitle">Documentos</p>
                            </article>
                        </div>
                    </div>
                </section>
                        </div>
                    </div>
                </div>

        </section>
        <script async type="text/javascript" src="../js/bulma.js"></script>
             <!-- Developers -->

        <div class="section">
            <div class="box">
                <div class="field has-addons">
                    <div class="control is-expanded">
                        <input class="input has-text-centered" type="search" placeholder="» » » » » » find me « « « « « «">
                    </div>
                    <div class="control">
                        <a class="button is-info">Search</a>
                    </div>
                </div>
            </div>
            <!-- Developers -->
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">Livros</h1>
                    <a id="livros" name="livros">
                </div>
            </div>
            <div class="row columns">
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="https://images.unsplash.com/photo-1475778057357-d35f37fa89dd?dpr=1&auto=compress,format&fit=crop&w=1920&h=&q=80&cs=tinysrgb&crop=" alt="Image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="https://i.imgsafe.org/a4/a4bb9acc5e.jpeg" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Okinami</p>
                                    <p><span class="title is-6"><a href="http://twitter.com/#">@twitterid</a></span></p>
                                    <p class="subtitle is-6">Lead Developer</p>
                                </div>
                            </div>
                            <div class="content">
                                The Beast stumbled in the dark for it could no longer see the path. It started to fracture and weaken, trying to reshape itself into the form of metal. Even the witches would no longer lay eyes upon it, for it had become hideous and twisted.
                                <div class="background-icon"><span class="icon-twitter"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="https://source.unsplash.com/uzDLtlPY8kQ" alt="Image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="https://cdn.discordapp.com/avatars/244932903530659841/0c924a19fcf1b5c59bc9dc1b58b61bb0.jpg?size=1024" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">McSocks</p>
                                    <p><span class="title is-6"><a href="http://twitter.com/#">@twitterid</a></span></p>
                                    <p class="subtitle is-6">Developer</p>
                                </div>
                            </div>
                            <div class="content">
                                The soul of the Beast seemed lost forever. Then, by the full moon's light, a child was born; child with the unbridled soul of the Beast that would make all others pale in comparison.
                                <div class="background-icon"><span class="icon-facebook"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-one-third">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="https://source.unsplash.com/pe_R74hldW4" alt="Image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-left">
                                    <figure class="image is-96x96">
                                        <img src="https://i.imgsafe.org/a5/a5e978ce20.jpeg" alt="Image">
                                    </figure>
                                </div>
                                <div class="media-content">
                                    <p class="title is-4 no-padding">The Conceptionist</p>
                                    <p><span class="title is-6"><a href="http://twitter.com/#">@twitterid</a></span></p>
                                    <p class="subtitle is-6">Developer</p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-barcode"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Developers -->
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">Autores</h1>
                    <a id="autores" name="autores">
                </div>
            </div>

            <!-- Staff -->
            <div id="people" class="row columns is-multiline"></div>
            <!-- End Staff -->
        </div>
            <section class="hero is-info is-medium is-bold">
    </section>

    </div>

<!-- identification -->
    % include('footer.tpl')
<!-- end identification -->

            <script src="../js/bulma.js"></script>


</body>

</html>