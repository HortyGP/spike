import bottle
import os
import sys

from bottle import TEMPLATE_PATH, static_file, route, default_app, get
project_home = os.path.dirname(os.path.abspath(__file__))

# add your project directory to the sys.path
# project_home = u'/home/supygirls/dev/SuPyGirls/src'
if project_home not in sys.path:
    sys.path = [project_home] + sys.path

application = default_app()

# Create a new list with absolute paths
# TEMPLATE_PATH.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../view/tpl')))
# make sure the default templates directory is known to Bottle
tpl_dir = os.path.join(project_home, 'view/tpl')
img_dir = os.path.join(project_home, 'view/image')
css_dir = os.path.join(project_home, 'view/css')
js_dir = os.path.join(project_home, 'view/js')
py_dir = os.path.join(project_home, 'view/bies')

if tpl_dir not in TEMPLATE_PATH:
    TEMPLATE_PATH.insert(0, tpl_dir)


@route('/')
@bottle.view('cards')
def index():
    return {}


# Static Routes
@get("/file/BibliotecaIES/<filepath:re:.*\.(png|jpg|svg|gif|ico)>")
def img(filepath):
    return static_file(filepath, root=img_dir)


# Static Routes
@get("/file/BibliotecaIES/<filepath:re:.*\.css>")
def ajs(filepath):
    return static_file(filepath, root=css_dir)


# Static Routes
@get("/file/BibliotecaIES/<filepath:re:.*\.js>")
def ajs(filepath):
    return static_file(filepath, root=js_dir)


# Static Routes
@get("/file/BibliotecaIES/<filepath:re:.*\.py>")
def ajs(filepath):
    return static_file(filepath, root=py_dir)


if __name__ == "__main__":
    bottle.run(host='localhost', port=8080)
